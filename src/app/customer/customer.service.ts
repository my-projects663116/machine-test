import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }


  cstlist(){
    return this.http.get(environment.apiUrl+`/users`);
  }

  addcus(data:any){
    return this.http.post(environment.apiUrl+`/users`,data);
  }

  cstdetails(id:number){
    return this.http.get(environment.apiUrl+`/users/`+id);
  }

  cstdelete(id:number){
    return this.http.delete(environment.apiUrl+`/users/`+id);
  }


}
