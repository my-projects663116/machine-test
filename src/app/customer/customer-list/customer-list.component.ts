import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { CustomerService } from '../customer.service';
import {MatDialog,MatDialogModule} from '@angular/material/dialog';
import { CustomerDeleteComponent } from '../customer-delete/customer-delete.component';


@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  dataSource:any;
  displayedColumns = ['name', 'email', 'phone', 'website','action'];
  constructor(private customerserv : CustomerService,public dialog: MatDialog) { }
 
  
 
  @ViewChild(MatPaginator) paginator: any;

  // ngAfterViewInit() {
  //   this.dataSource = this.paginator;
  // }

  ngOnInit(): void {
    this.customerlist()
  }


  applyFilter(event: Event) {
    console.log(event)
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue)
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  customerlist(){
    console.log('cus')
this.customerserv.cstlist().subscribe((res=>{
  console.log(res,'result')
  this.dataSource = res
}))
  }

  openDialog(id:number) {
    const dialogRef = this.dialog.open(CustomerDeleteComponent,{
      height: '250px',
      width: '500px',
      data: { cusId: id },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
