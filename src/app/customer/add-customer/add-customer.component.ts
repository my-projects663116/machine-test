import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  addForm!:FormGroup

  constructor(private formBuilder:FormBuilder,private router:Router, private cusserv:CustomerService) { }

  ngOnInit(): void {
    this.setForm()
  }

  setForm(){
    this.addForm = this.formBuilder.group({
      name: ['',[Validators.required]],
      email: ['',[Validators.required, Validators.email]],
      phone: ['',[Validators.required]],
      website: ['',[Validators.required]],
    })
    }

    addCustomer(){ 
      let params={
        name:this.addForm.value.name,
        email:this.addForm.value.email,
        phone:this.addForm.value.phone,
        website:this.addForm.value.website
      }
      console.log(params,'params')
      this.addForm.markAllAsTouched();
      if(this.addForm.valid){
        console.log(params,'params')
      this.cusserv.addcus(params).subscribe((result:any)=>{
        console.log("result",result)
        this.router.navigate(['/customer-list']);
        // if(result.status === '1'){
        //   console.log("result",result?.data)
        // }
        // else{
        //   console.log("result",result)
        // }
        
      },error=>{
        console.log("error",error)
      })
    }
  }

}
