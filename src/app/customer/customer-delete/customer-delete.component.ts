import { Component, Inject, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-delete',
  templateUrl: './customer-delete.component.html',
  styleUrls: ['./customer-delete.component.scss']
})
export class CustomerDeleteComponent implements OnInit {

  constructor(private cstserv:CustomerService,public dialogRef: MatDialogRef<CustomerDeleteComponent>,private router: Router, @Inject(MAT_DIALOG_DATA) public data: {cusId: number}) { }

  ngOnInit(): void {
    console.log(this.data.cusId);
  }

  deleteCustomer(){
    this.cstserv.cstdelete(this.data.cusId).subscribe(result=>{
      if(result){
        console.log("result",result)
      }
      else{
        console.log("something went wrong")
      }
    },
    (error) => {
      console.error('Error', error);
    })
  }

  close(){
    this.dialogRef.close()
  }

}
