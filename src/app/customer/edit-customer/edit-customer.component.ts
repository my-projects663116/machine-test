import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {

  addForm!:FormGroup
  cusId:any;
  cusDetails:any;

  constructor(private formBuilder:FormBuilder,private router:Router,private route:ActivatedRoute, private cusserv:CustomerService) { }

  ngOnInit(): void {
        this.route.params.subscribe((params:any)=>{
      if (params['id']!=undefined){
        this.cusId=params['id']
        console.log(this.cusId,'cusId')
      }
    })
    this.setForm()
    if(this.cusId){
this.getcus(this.cusId)
    }
  }

  setForm(){
    this.addForm = this.formBuilder.group({
      name: [this.cusDetails?.name || '',[Validators.required]],
      email: [this.cusDetails?.email || '',[Validators.required, Validators.email]],
      phone: [this.cusDetails?.phone || '',[Validators.required]],
      website: [this.cusDetails?.website || '',[Validators.required]],
    })
    }

    getcus(id:number){
      this.cusserv.cstdetails(id).subscribe((res=>{
        console.log(res,'result')
        if(res){
          this.cusDetails = res
           this.setForm()
        }
      }))
    }

    addCustomer(){ 
      let params={
        name:this.addForm.value.name,
        email:this.addForm.value.email,
        phone:this.addForm.value.phone,
        website:this.addForm.value.website
      }
      console.log(params,'params')
      this.addForm.markAllAsTouched();
      if(this.addForm.valid){
        console.log(params,'params')
      this.cusserv.addcus(params).subscribe((result:any)=>{
        console.log("result",result)
        this.router.navigate(['/customer-list']);

        // if(result.status === '1'){
        //   console.log("result",result?.data)
        // }
        // else{
        //   console.log("result",result)
        // }
        
      },error=>{
        console.log("error",error)
      })
    }
  }


}
